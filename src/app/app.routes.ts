import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { Component } from '../../node_modules/@angular/core';
import { HomeComponent } from './components/home/home.component';

export const AppRoutes: Routes = [
  {
    path:'',
    component: HomeComponent,
  },  
  {
      path: 'sign-in',
      component: LoginComponent ,
    },
    { path: '**', 
      component: PageNotFoundComponent,
    },
  
    
  ];
  